# Caronas UNB

O Caronas UNB é um projeto criado para facilitar a locomoção do professor e do aluno até a UNB. Nesta plataforma, os alunos e professores poderão postar caronas com destino a FGA, FCE e Darcy.

A finalidade do projeto é ajudar a vida do professor e do aluno que perdem muito tempo em transporte público para se locomover até os Campus existentes da UnB.

Iniciando o projeto:

Requisitos : Ruby(2.3.1) e Rails(5.2.0) instalado

```
mdkir caronas
cd caronas
git clone https://gitlab.com/lucasmmachado/Caronas_Fga_unb.git
cd Caronas_Fga_unb
rails db:drop db:create db:migrate
rails s

```
